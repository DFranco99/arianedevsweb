import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  navbarOptions = [
    "Feature",
    "Testimonials",
    "Integration",
    "Blog",
    "Inner Pages", 
  ];
  optionActive = "Feature";

  constructor() { }

  ngOnInit(): void {
  }

  selectOption(option: string): void{
    this.optionActive = option;
    console.log(this.optionActive)
  }
}
