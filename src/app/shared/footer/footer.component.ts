import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footerOptionsCompany = [
    "Donec dignissim",
    "Curabitur egestas",
    "Nam posuere",
    "Aenean facilisis"
  ];
  footerOptionsServices = [
    "Cras convallis",
    "Vestibulum faucibus",
    "Quisque lacinia purus",
    "Aliquam nec ex"
  ];
  footerOptionsResources = [
    "Suspendisse porttitor",
    "Nam posuere",
    "Curabitur egestas" 
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
